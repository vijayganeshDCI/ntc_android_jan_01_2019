package com.app.ntc.dagger;


import android.content.SharedPreferences;

import com.app.ntc.application.NtcAppplication;
import com.app.ntc.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    void inject(NtcAppplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();
}
