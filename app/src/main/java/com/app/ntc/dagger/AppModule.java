package com.app.ntc.dagger;


import android.content.Context;
import android.content.SharedPreferences;

import com.app.ntc.application.NtcAppplication;
import com.app.ntc.retrofit.NtcAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


//provides instance or objects for a class
@Module
public class AppModule {

    public static final String NTC_PREFS = "ntc";

    private final NtcAppplication ntcApp;

    public AppModule(NtcAppplication app) {
        this.ntcApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return ntcApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return ntcApp.getSharedPreferences(NTC_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public NtcAPI provideDMKApiInterface(Retrofit retrofit) {
        return retrofit.create(NtcAPI.class);
    }


}
