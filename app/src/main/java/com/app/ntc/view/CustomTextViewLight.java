package com.app.ntc.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomTextViewLight extends android.support.v7.widget.AppCompatTextView {
    public CustomTextViewLight(Context context) {
        super(context);
        setFont();
    }

    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fontName/proximanova_light.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}
