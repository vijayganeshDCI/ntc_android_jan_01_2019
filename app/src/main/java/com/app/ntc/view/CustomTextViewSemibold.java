package com.app.ntc.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomTextViewSemibold extends android.support.v7.widget.AppCompatTextView {
    public CustomTextViewSemibold(Context context) {
        super(context);
        setFont();
    }

    public CustomTextViewSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomTextViewSemibold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fontName/proximanova_semibold.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}
