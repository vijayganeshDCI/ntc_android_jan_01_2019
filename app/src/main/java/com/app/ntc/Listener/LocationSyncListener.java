package com.app.ntc.Listener;

public interface LocationSyncListener {

    public void getLocationDetails(String latlng);
}
