package com.app.ntc.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.app.ntc.BuildConfig;
import com.app.ntc.dagger.AppModule;
import com.app.ntc.dagger.ApplicationComponent;
import com.app.ntc.dagger.DaggerApplicationComponent;
import com.app.ntc.retrofit.RetrofitModule;

import javax.inject.Inject;

public class NtcAppplication extends MultiDexApplication {

    @Inject
    public SharedPreferences mPrefs;
    private static NtcAppplication mInstance;


    public static NtcAppplication getContext() {
        return mInstance;
    }

    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BuildConfig.NTC_BASE_URL))
                .build();
        mComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static NtcAppplication from(@NonNull Context context) {
        return (NtcAppplication) context.getApplicationContext();
    }
    public static NtcAppplication get()
    {
        return mInstance;
    }

}
