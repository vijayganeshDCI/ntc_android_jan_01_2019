package com.app.ntc.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.app.ntc.R;

public class PermissionManager {

    private int REQUEST_STORAGE = 1;
    private String[] PERMISSIONSLIST;
    private Context context;


    public PermissionManager(Context context) {
        this.context = context;
    }


    public void requestPermission(String[] PERMISSIONS) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PERMISSIONSLIST = PERMISSIONS;
            ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, REQUEST_STORAGE);
        }
    }

    public void permissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE) {
            for (int i = 0; i < PERMISSIONSLIST.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    //permission allowed
                } else if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, PERMISSIONSLIST[i])) {
                    //Deny
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setMessage(R.string.app_name);
                    alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermission(PERMISSIONSLIST);
                        }
                    });
                    alertDialog.show();
                    break;
                } else {
                    //Deny with Dont show again
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setMessage(R.string.app_name);
                    alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                            intent.setData(uri);
                            context.startActivity(intent);
                        }
                    });
                    alertDialog.show();
                    break;
                }
            }


        }
    }


}
