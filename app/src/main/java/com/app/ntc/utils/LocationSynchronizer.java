package com.app.ntc.utils;

import android.content.Context;
import android.os.Handler;

import com.app.ntc.Listener.LocationSyncListener;

public class LocationSynchronizer {

    private Context context;
    private LocationManager locationManager;
    private Handler handler = new Handler();
    private Runnable runnableCode;
    private long delayMiliSec = 50000;
    private LocationSyncListener locationSyncListener;


    public LocationSynchronizer(Context context) {
        this.context = context;
        this.locationSyncListener = (LocationSyncListener) context;
        locationManager = new LocationManager(context);
    }


    public void startLocationUpdate() {
        runnableCode = new Runnable() {
            @Override
            public void run() {
                locationManager.getLocation();
                if (locationManager.mLat != 0.0 && locationManager.mLng != 0.0)
                    locationSyncListener.getLocationDetails(locationManager.mLat + "" + locationManager.mLng);
                handler.postDelayed(runnableCode, delayMiliSec);
            }
        };
        handler.post(runnableCode);
    }

    public void stopLocationUpdate() {
        if (handler != null) {
            locationManager.disConnectGoogleApiClient();
            handler.removeCallbacks(runnableCode);
        }

    }


}
