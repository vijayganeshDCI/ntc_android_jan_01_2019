package com.app.ntc.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.app.ntc.Listener.LocationSyncListener;
import com.app.ntc.R;
import com.app.ntc.activity.BaseActivity;
import com.app.ntc.utils.LocationManager;
import com.app.ntc.utils.LocationSynchronizer;
import com.app.ntc.utils.PermissionManager;

public class MainActivity extends BaseActivity implements LocationSyncListener{

    private LocationManager locationManager;
    private LocationSynchronizer locationSynchronizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = new LocationManager(this);
        locationSynchronizer=new LocationSynchronizer(this);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationSynchronizer.startLocationUpdate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //check gps location on off
        locationManager.locationResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationSynchronizer.stopLocationUpdate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void getLocationDetails(String latlng) {
        //api call
        Toast.makeText(this,latlng,Toast.LENGTH_SHORT).show();
    }
}
